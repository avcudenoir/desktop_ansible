call plug#begin()
Plug 'preservim/nerdtree'
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
Plug 'dikiaap/minimalist'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-markdown'
Plug 'ntpeters/vim-better-whitespace'
Plug 'hashivim/vim-terraform'
Plug 'pearofducks/ansible-vim'
Plug 'elzr/vim-json'
Plug 'chr4/nginx.vim'
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'preservim/nerdtree'
Plug 'tpope/vim-sensible'
Plug 'vim-airline/vim-airline'
Plug 'morhetz/gruvbox'
Plug 'vim-syntastic/syntastic'
Plug 'ryanoasis/vim-devicons'
call plug#end()

" map the toggle
map <C-n> :NERDTreeToggle<CR>

" control tabs
nnoremap <C-t>n :tabnew<CR>
nnoremap <C-t>d :tabc
nnoremap <C-t><up> :tabr<CR>
nnoremap <C-t><down> :tabl<CR>
nnoremap <C-t><left> :tabp<CR>
nnoremap <C-t><right> :tabn<CR>

nnoremap th  :tabfirst<CR>
nnoremap tk  :tabnext<CR>
nnoremap tj  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnew<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>

" open NERDTree automatically when vim starts up on opening a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" Allow vim-terraform to automatically format *.tf and *.tfvars files with terraform fmt.
let g:terraform_fmt_on_save=1

let g:gruvbox_contrast_dark = 'hard'
let g:airline_theme = 'gruvbox'
colorscheme gruvbox

" show line numbers
set number

" Reload vims configuration file
nnoremap crel :source $MYVIMRC<CR>

" Mouse wheel
set mouse=a
map <ScrollWheelUp> <C-Y>
map <ScrollWheelDown> <C-E>

" yank into os clipboard
set clipboard=unnamed

set encoding=UTF-8
